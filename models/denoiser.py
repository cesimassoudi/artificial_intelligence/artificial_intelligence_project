import os
import random
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from mimetypes import guess_extension
from tqdm import tqdm
from keras_tuner.tuners import RandomSearch,BayesianOptimization
from keras_tuner import Objective
import keras_tuner as kt
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.regularizers import l1, l2

data_dir = "data"
class_labels = ['Painting', 'Photo', 'Schematics', 'Sketch', 'Text']
checkpoint_dir = "tmp/ckpt/ae_model"
checkpoint_file = os.path.join(checkpoint_dir, 'ae_model_First_cpkt.h5')

def model(image_h,image_w):
    model_input = layers.Input(shape=(None, None, 3))
    preprocess = layers.Resizing(image_h,image_w)(model_input)
    encoder_input = layers.Rescaling(scale=1./255)(preprocess)
    
    
    x = layers.Conv2D(128, 3, padding='same', input_shape=(image_h,image_w, 2), kernel_regularizer=l1(0.01) ,activation="relu")(encoder_input)
    x = layers.MaxPooling2D(3, padding='same')(x)

    x = layers.Dropout(0.4)(x)
    x = layers.Conv2D(256, 3, padding='same', activation="relu")(x)
    x = layers.MaxPooling2D(3, padding='same')(x)

    x = layers.Conv2D(512, 3, padding='same', activation="relu")(x)
    x = layers.MaxPooling2D(3, padding='same')(x)

    x = layers.Conv2D(1024, 3, padding='same', activation="relu")(x)
    encoder = layers.Conv2D(1024, 3, padding='same', activation="relu")(x)
    xbar = encoder

    xbar = layers.Conv2D(512, 3, padding='same', activation="relu")(xbar)
    xbar = layers.UpSampling2D(3)(xbar)
    
    xbar = layers.Conv2D(256, 3, padding='same', activation="relu")(xbar)
    xbar = layers.UpSampling2D(3)(xbar)

    xbar = layers.Dropout(0.4)(xbar)
    xbar = layers.Conv2D(128, 3, padding='same', kernel_regularizer=l2(0.01), activation="relu")(xbar)
    xbar = layers.UpSampling2D(3)(xbar)

    output = layers.Conv2D(3, 3, padding='same', activation="sigmoid")(xbar)

    return keras.Model(model_input, output, name="AutoEncoder")


def model_build_tuner(hp,image_h, image_w, kernel_size,Encoder_Config,Decoder_Config):
    model_input = layers.Input(shape=(None, None, 3))
    preprocess = layers.Resizing(image_h,image_w)(model_input)
    encoder_input = layers.Rescaling(scale=1./255)(preprocess)
    
    x = encoder_input
    CNNLayersTuning = []

    for i in range(len(Encoder_Config)):
        cur = Encoder_Config[i]
        if(cur[0]):
            dropconfig = cur[0]
            if(dropconfig[0]):
                dropvalue = hp.Float(f"encoder_dropout_bloc_{i}", min_value=dropconfig[1], max_value=dropconfig[2], step=dropconfig[3])
            else:
                dropvalue = dropconfig[1]
            
            x = layers.Dropout(dropvalue)(x)

        convconfig = cur[1]
        if(convconfig[0]):
            filtercount = hp.Int(f"convolution_bloc_{i}", min_value=convconfig[1], max_value=convconfig[2], step=convconfig[3])
        else:
            filtercount = convconfig[1]

        CNNLayersTuning.append(filtercount)
        
        x = layers.Conv2D(filtercount, kernel_size, activation="relu", padding='same')(x)

        x = layers.MaxPooling2D(kernel_size, padding='same')(x)

    encoded = layers.Conv2D(
        hp.Int(f"convolution_latent", min_value=32, max_value=64, step=8),
        kernel_size,
        activation="relu",
        padding='same')(x)

    xbar = encoded

    for i in reversed(range(len(CNNLayersTuning))):
        filtercount = CNNLayersTuning[i]
        index = len(CNNLayersTuning) - i - 1
        if(Decoder_Config[index]):
            cur = Decoder_Config[index]
            if(cur[0]):
                dropvalue = hp.Float(f"decoder_dropout_bloc_{index}", min_value=cur[1], max_value=cur[2], step=cur[3])
            else:
                dropvalue = cur[1]
            
            xbar = layers.Dropout(dropvalue)(xbar)

        xbar = layers.Conv2D(filtercount,kernel_size, padding='same', activation="relu")(xbar)
        xbar = layers.UpSampling2D(kernel_size)(xbar)

    output = layers.Conv2D(3, 3, activation="relu", padding='same')(xbar)

    compiled_model = keras.models.Model(model_input, output)
    compiled_model.compile(optimizer=keras.optimizers.Adam(learning_rate=0.001),
                          loss='mean_squared_error')
    # compiled_model.summary()
    return compiled_model


def guess_image_mime_type(f):
    '''
    Function guesses an image mime type.
    Supported filetypes are JPG, BMP, PNG.
    '''
    with open(f, 'rb') as file:
        data = file.read(12)

    if data[:3] == b'\xff\xd8\xff':
        return 'image/jpeg'
    elif data[1:4] == b'PNG':
        return 'image/png'
    elif data[:2] == b'BM':
        return 'image/bmp'
    elif data[:3] == b'GIF':
        return 'image/gif'
    elif data[:4] == b'II*\x00' or data[:4] == b'MM\x00*':
        return 'image/tiff'
    elif data[:4] == b'RIFF' and data[8:12] == b'WEBP':
        return 'image/webp'
    else:
        return 'image/unknown-type'

def load_dataset(dataset:str):
    # Load images and labels into lists
    image_paths = []
    dataset_path = os.path.join(data_dir, dataset)
    for filename in tqdm(os.listdir(dataset_path)):

        image_path = os.path.join(data_dir, dataset, filename)
        trueExtension = guess_extension(guess_image_mime_type(image_path))
        
        if trueExtension == ".jpg" or trueExtension == ".png":  # Add more file extensions if necessary
            filenamewithoutExtension = filename.split('.')[0]
            truefilename = filenamewithoutExtension+trueExtension
            
            true_image_path = os.path.join(data_dir, dataset, truefilename)
            if(image_path != true_image_path):
                if os.path.exists(true_image_path):
                    os.remove(true_image_path)
                os.rename(image_path, true_image_path)
                # print(image_path + " Changed to " + true_image_path)


            image_paths.append(true_image_path)

    return image_paths

def noiseimgtf(img, image_h, image_w):
    
    noise_factor = 0.6
    noiseunclipped =  img + noise_factor*tf.random.normal(shape=(image_h,image_w,3), dtype=tf.float32)
    return tf.clip_by_value(noiseunclipped, 0, 1)

def load_image(image_data,image_h,image_w):
    """
    La fonction load_image a pour entrée le chemin d'une image et pour sortie un couple
    contenant l'image traitée ainsi que son chemin d'accès.
    La fonction load_image effectue les traitement suivant:
        1. Chargement du fichier correspondant au chemin d'accès image_path
        2. Décodage de l'image en RGB.
    """

    # Chargement du fichier d'image
    img = tf.io.read_file(image_data['image'])

    # Décodage de l'image en RGB
    img = tf.image.decode_image(img, channels=3,expand_animations=False)
    
    img.set_shape([None,None,3])

    # Rescale de l'image
    img = tf.image.resize(img, [image_h, image_w])
    
    img = img / 255.

    imgnoisy = noiseimgtf(img, image_h, image_w) # Add Noise

    return imgnoisy, img

def data_preprocessing(
        dataset="",
        image_h=256,
        image_w=256,
        batch_s=32
        ):
    # Get image paths
    image_paths = load_dataset(dataset)

    # 
    X_train_setdf, X_test_setdf = train_test_split(
        image_paths, train_size=0.8, shuffle=True)

    # 
    train_set = tf.data.Dataset.from_tensor_slices({'image': X_train_setdf})
    test_set = tf.data.Dataset.from_tensor_slices({'image': X_test_setdf})
    
    # dataset loading from image paths
    train_set_image = train_set.map(
        (lambda x : load_image(x,image_h,image_w))
        , num_parallel_calls=tf.data.AUTOTUNE).batch(batch_s)
        
    test_set_image = test_set.map(
        (lambda x : load_image(x,image_h,image_w))
        , num_parallel_calls=tf.data.AUTOTUNE).batch(batch_s)
    
    return train_set_image, test_set_image
    

def denoiser(
        ae_Model=None,
        dataset="noisy",
        epochs=10,
        image_h=256,
        image_w=256,
        batch_s=32
        ):
    
    print("autoencoder model")
    print()
    
    train_set_image, test_set_image = data_preprocessing(
        dataset=dataset,
        image_h=image_h,
        image_w=image_w,
        batch_s=batch_s
        )

    model_checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_file,
        save_weights_only=False,
        save_best_only=True,
        save_freq='epoch',
        verbose=1
    )

    earlystopping_callback = EarlyStopping(
        monitor='val_loss',
        patience=7,
        mode='auto',
        restore_best_weights=True,
    )

    try:
        cpktmodel = tf.keras.models.load_model(checkpoint_file)
        choice = input("Do you want to load from checkpoint ? y/[N] :\n")
        if choice =='y':
            ae_Model = cpktmodel
            print("Model loaded from the latest checkpoint.")
        else:
            os.remove(checkpoint_file)
    except Exception as e:
        print("No checkpoint found. Training the model from scratch.")
    
    # Model building
    if ae_Model == None:        
        ae_Model = model(image_h,image_w)
        optimizer = tf.keras.optimizers.Adam()
        # Model compilation
        ae_Model.compile(
            optimizer=optimizer, 
            loss="mean_squared_error"
        )
    
        ae_Model.build((None, None, 3))
    else:
        choice = input("Do you want to recompile ? y/[N] :\n")
        if choice =='y':
            optimizer = tf.keras.optimizers.Adam()
            # Model compilation
            ae_Model.compile(
                optimizer=optimizer, 
                loss="mean_squared_error"
            )
    
            ae_Model.build((None, None, 3))
    

    ae_Model.summary()
    
    continue_fit = True
    historyres = None
    while continue_fit:
        # Model training
        history = ae_Model.fit(
            train_set_image,
            validation_data = test_set_image,
            epochs = epochs,
            callbacks=[model_checkpoint_callback, earlystopping_callback]
        )
        if not historyres:
            historyres = history
        else :
            for key in history.history.keys():
                if not(key in historyres.history):
                    historyres.history[key] = []
                historyres.history[key].extend(history.history[key])

        choice = input(f"Do you want to continue training on {epochs} more ? y/[N]")
        continue_fit = (choice == 'y')

    os.remove(checkpoint_file)

    return ae_Model, historyres


def denoiser_tuner(
        dataset="noisy",
        epochs=10,
        image_h=256,
        image_w=256,
        batch_s=32
        ):

    print("autoencoder model tuner")
    print()

    layerinput = input("How many layer do you want ? [int] Default 2:\n")
    layer_count = int(layerinput) if layerinput.isdigit() else 2

    layerinput = input("What kernel size do you want ? [int] Default 2:\n")
    kernel_size = int(layerinput) if layerinput.isdigit() else 2
    
    multipleneeded = (kernel_size**layer_count)

    if (image_h%multipleneeded):
        image_h = image_h + (multipleneeded - (image_h%multipleneeded))

    if (image_w%multipleneeded):
        image_w = image_w + (multipleneeded - (image_w%multipleneeded))

    print(f"Size compatible with model {image_h} x {image_w}")

    Encoder_Config = []

    for i in range(layer_count):
        print(f"Configuring encoder bloc {i}")
        dropconfig = None
        choice = input("Do you want to add dropout ? y/[N] :\n")

        if(choice == 'y'):
            print(f"Configuring encoder drop out {i}")
            choice = input("Do you want to tune ? y/[N] :\n")
            dropvalue = 0.2
            if(choice == 'y'):
                dropinput = input("Which min value to give ?[0-1] (default: 0.2) :\n")
                mindropvalue = float(dropinput) if dropinput.isnumeric() else 0.2

                dropinput = input("Which max value to give ?[0-1] (default: 0.3) :\n")
                maxdropvalue = float(dropinput) if dropinput.isnumeric() else 0.3

                dropinput = input("Which step value to give ?[0-1] (default: 0.05) :\n")
                stepdropvalue = float(dropinput) if dropinput.isnumeric() else 0.05
                dropconfig = (True, mindropvalue, maxdropvalue, stepdropvalue)
            else:
                dropinput = input("Which value to give ?[0-1] (default: 0.2) :\n")
                dropvalue = float(dropinput) if dropinput.isnumeric() else 0.2
                dropconfig = (False, dropvalue)

        choice = input(f"Do you want to tune the Convultion Layer {i} ? y/[N] :\n")
        filtercount = 32

        convconfig = None
        if(choice == 'y'):
            dropinput = input("Which min filternumber to give ?[int] (default: 32) :\n")
            minfilternumbervalue = float(dropinput) if dropinput.isdigit() else 32

            dropinput = input("Which max filternumber to give ?[int] (default: 64) :\n")
            maxfilternumbervalue = float(dropinput) if dropinput.isdigit() else 64

            dropinput = input("Which step filternumber to give ?[int] (default: 8) :\n")
            stepfilternumbervalue = float(dropinput) if dropinput.isdigit() else 8

            convconfig = (True, minfilternumbervalue, maxfilternumbervalue, stepfilternumbervalue)
        else:
            dropinput = input("Which filtercount to give ?[int] (default: 32) :\n")
            filtercount = float(dropinput) if dropinput.isnumeric() else 32
            convconfig = (False, filtercount)
        Encoder_Config.append((dropconfig,convconfig))
    
    Decoder_Config = []

    for index in range(layer_count):
        print(f"Configuring decoder bloc {index}")

        choice = input("Do you want to add dropout ? y/[N] :\n")
        dropconfig = None
        if(choice == 'y'):
            print(f"Configuring decoder drop out {index}")
            choice = input("Do you want to tune ? y/[N] :\n")
            dropvalue = 0.2
            if(choice == 'y'):
                dropinput = input("Which min value to give ?[0-1] (default: 0.2) :\n")
                mindropvalue = float(dropinput) if dropinput.isnumeric() else 0.2

                dropinput = input("Which max value to give ?[0-1] (default: 0.3) :\n")
                maxdropvalue = float(dropinput) if dropinput.isnumeric() else 0.3

                dropinput = input("Which step value to give ?[0-1] (default: 0.05) :\n")
                stepdropvalue = float(dropinput) if dropinput.isnumeric() else 0.05

                dropconfig = (True, mindropvalue, maxdropvalue, stepdropvalue)
            else:
                dropinput = input("Which value to give ?[0-1] (default: 0.2) :\n")
                dropvalue = float(dropinput) if dropinput.isnumeric() else 0.2
                dropconfig = (False, dropvalue)
        Decoder_Config.append(dropconfig) 
   


    train_set_image, test_set_image = data_preprocessing(
        dataset=dataset,
        image_h=image_h,
        image_w=image_w,
        batch_s=batch_s
        )

    custom_objective = Objective("val_loss", direction="min")
    
    # Create a Keras Tuner RandomSearch tuner
    tuner = BayesianOptimization(
        (lambda x: model_build_tuner(x,image_h,image_w,kernel_size,Encoder_Config,Decoder_Config)),
        overwrite=True,
        objective=custom_objective,
        max_trials=50,
        directory='TuningMetadata',
        project_name='denoiser'
    )

    # Perform the hyperparameter search
    tuner.search(
        train_set_image,
        epochs=epochs,
        validation_data=test_set_image
    )

    # Get the best model
    best_model = tuner.get_best_models(1)[0]

    return best_model, None

def get_noise_img(
        model, 
        nb=10,
        img_seed=42,
        ):
    
    print("Denoiser predict")

    if model == None:
        print("You need a model for this action")
        return

    random.seed(img_seed)

    image_paths = load_dataset("noisy")
    rand_img_path = random.sample(image_paths, nb)

    img_set = tf.data.Dataset.from_tensor_slices({'image': rand_img_path})
    
    # dataset loading from image paths
    noise_set = img_set.map(
        (lambda x : load_image(x,256,256))
        , num_parallel_calls=tf.data.AUTOTUNE).batch(32)


    img_res, img_noise = [], []
    for noise, img in noise_set:
        img_res.append(img)
        img_noise.append(noise)

    decoded_img = model.predict(img_noise)

    return img_res, img_noise, decoded_img

# if __name__ == "__main__":
#     binary_classification()
