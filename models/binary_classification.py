import os
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from mimetypes import guess_extension
from tqdm import tqdm
from keras_tuner.tuners import RandomSearch,BayesianOptimization
from keras_tuner import Objective
import keras_tuner as kt

data_dir = "data"
class_labels = ['Painting', 'Photo', 'Schematics', 'Sketch', 'Text']

def model(image_h,image_w):
    model_input = layers.Input(shape=(None, None, 3))
    model = layers.Resizing(image_h,image_w)(model_input)
    model = layers.experimental.preprocessing.RandomFlip("horizontal", 
        input_shape=(image_h, image_w, 3))(model)
    model = layers.experimental.preprocessing.RandomRotation(0.3)(model)
    model = layers.experimental.preprocessing.RandomZoom(0.3)(model)
    model = layers.Rescaling(scale=1./255)(model)
    model = layers.Conv2D(16, (3,3),input_shape=(image_h,image_w, 3) ,activation="relu")(model)
    model = layers.MaxPooling2D((3, 3))(model)
    # model = layers.Conv2D(32, (3,3), activation="relu")(model)
    # model = layers.MaxPooling2D((3, 3))(model)
    # model = layers.Conv2D(64, (3,3), activation="relu")(model)
    # model = layers.MaxPooling2D((3, 3))(model)
    model = layers.Flatten()(model)
    model = layers.Dropout(0.3, input_shape=(image_h,image_w, 3))(model)
    model = layers.Dense(128, activation="relu")(model)
    model = layers.Dropout(0.2, input_shape=(image_h,image_w, 3))(model)
    # model = layers.Dense(128, activation="relu")(model)
    output = layers.Dense(2, activation="softmax")(model)

    return keras.Model(model_input, output, name="binary_classification")


def model_build_tuner(hp,image_h,image_w):
    model_input = layers.Input(shape=(image_h, image_w, 3))
    model = layers.experimental.preprocessing.Rescaling(scale=1./255)(model_input)

    # First Convolutional Layer
    model = layers.Conv2D(
        filters=hp.Int('conv_1_filter', min_value=50, max_value=128, step=8),
        kernel_size=3,
        activation='relu'
    )(model)
    model = layers.MaxPooling2D(3)(model)

    # Second Convolutional Layer
    model = layers.Conv2D(
        filters=hp.Int('conv_2_filter', min_value=50, max_value=128, step=8),
        kernel_size=3,
        activation='relu'
    )(model)
    model = layers.MaxPooling2D(3)(model)

    # Third Convolutional Layer
    model = layers.Conv2D(
        filters=hp.Int('conv_3_filter', min_value=8, max_value=50, step=8),
        kernel_size=3,
        activation='relu'
    )(model)
    model = layers.MaxPooling2D(3)(model)

    model = layers.Flatten()(model)

    model = layers.Dropout(0.3)(model)

    model = layers.Dense(
        units=hp.Int('dense_1_units', min_value=300, max_value=1024, step=16),
        activation='relu'
    )(model)


    model = layers.Dropout(hp.Float('dropout_1', min_value=0.1, max_value=0.5, step=0.1))(model)

    model = layers.Dense(
        units=hp.Int('dense_2_units', min_value=100, max_value=512, step=16),
        activation='relu'
    )(model)

    model = layers.Dropout(hp.Float('dropout_2', min_value=0.1, max_value=0.5, step=0.1))(model)

    output = layers.Dense(2, activation="softmax")(model)

    compiled_model = keras.models.Model(model_input, output)
    compiled_model.compile(optimizer=keras.optimizers.Adam(learning_rate=0.001),
                          loss='binary_crossentropy',
                          metrics=['Recall'])

    return compiled_model


def guess_image_mime_type(f):
    '''
    Function guesses an image mime type.
    Supported filetypes are JPG, BMP, PNG.
    '''
    with open(f, 'rb') as file:
        data = file.read(12)

    if data[:3] == b'\xff\xd8\xff':
        return 'image/jpeg'
    elif data[1:4] == b'PNG':
        return 'image/png'
    elif data[:2] == b'BM':
        return 'image/bmp'
    elif data[:3] == b'GIF':
        return 'image/gif'
    elif data[:4] == b'II*\x00' or data[:4] == b'MM\x00*':
        return 'image/tiff'
    elif data[:4] == b'RIFF' and data[8:12] == b'WEBP':
        return 'image/webp'
    else:
        return 'image/unknown-type'

def load_dataset(dataset:str):
    # Load images and labels into lists
    image_paths = []
    labels = []

    # Iterate through the class labels and load images
    for label in class_labels:
        print("Retriving "+ label +" Dataset")
        label_dir = os.path.join(data_dir, dataset, label)
        
        for filename in tqdm(os.listdir(label_dir)):

            image_path = os.path.join(label_dir, filename)
            trueExtension = guess_extension(guess_image_mime_type(image_path))
            
            if trueExtension == ".jpg" or trueExtension == ".png":  # Add more file extensions if necessary
                filenamewithoutExtension = filename.split('.')[0]
                truefilename = filenamewithoutExtension+trueExtension
                
                true_image_path = os.path.join(label_dir, truefilename)
                if(image_path != true_image_path):
                    if os.path.exists(true_image_path):
                        os.remove(true_image_path)
                    os.rename(image_path, true_image_path)
                    # print(image_path + " Changed to " + true_image_path)


                image_paths.append(true_image_path)
                labels.append((1 if label=='Photo' else 0))

    return image_paths, labels

def load_image(image_data,image_h,image_w):
    """
    La fonction load_image a pour entrée le chemin d'une image et pour sortie un couple
    contenant l'image traitée ainsi que son chemin d'accès.
    La fonction load_image effectue les traitement suivant:
        1. Chargement du fichier correspondant au chemin d'accès image_path
        2. Décodage de l'image en RGB.
    """

    # Chargement du fichier d'image
    img = tf.io.read_file(image_data['image'])

    # Décodage de l'image en RGB
    img = tf.image.decode_image(img, channels=3,expand_animations=False)
    
    img.set_shape([None,None,3])

    # Rescale de l'image
    img = tf.image.resize(img, [image_h, image_w])

    # Catégories (Photo et Non Photo)
    photo_category = tf.cast(image_data['Photo'], tf.int32)
    non_photo_category = 1-tf.cast(image_data['Photo'], tf.int32)

    # Transformation des étiquettes
    label = tf.stack([photo_category, non_photo_category], axis=-1)

    return img, label

def data_preprocessing(
        dataset="",
        image_h=256,
        image_w=256,
        batch_s=32
        ):
    # Get image paths
    image_paths, labels = load_dataset(dataset)

    # 
    X_train_setdf, X_test_setdf, y_train_setdf, y_test_setdf = train_test_split(
        image_paths,labels, train_size=0.8, shuffle=True)

    # 
    train_set = tf.data.Dataset.from_tensor_slices(
        {'image': X_train_setdf, 'Photo': y_train_setdf})
    test_set = tf.data.Dataset.from_tensor_slices(
        {'image': X_test_setdf, 'Photo': y_test_setdf})

    # dataset loading from image paths
    train_set_image = train_set.map(
        (lambda x : load_image(x,image_h,image_w))
        , num_parallel_calls=tf.data.AUTOTUNE).batch(batch_s)
        
    test_set_image = test_set.map(
        (lambda x : load_image(x,image_h,image_w))
        , num_parallel_calls=tf.data.AUTOTUNE).batch(batch_s)
    
    return train_set_image, test_set_image
    

def binary_classification(
        model_binary_classification=None,
        dataset="",
        epochs=10,
        image_h=256,
        image_w=256,
        batch_s=32
        ):
    print("binary classication model")
    print()
    
    train_set_image, test_set_image = data_preprocessing(
        dataset=dataset,
        image_h=image_h,
        image_w=image_w,
        batch_s=batch_s
        )
    
    # Model building
    if model_binary_classification == None:        
        model_binary_classification = model(image_h,image_w)
    
    # Model compilation
    model_binary_classification.compile(
        optimizer='adam', 
        loss= "binary_crossentropy",
        metrics=['Recall']
    )

    model_binary_classification.build((None, None, 3))

    model_binary_classification.summary()

    # Model training
    history = model_binary_classification.fit(
        train_set_image,
        validation_data = test_set_image,
        epochs = epochs
    )

    return model_binary_classification, history


def binary_classification_tuner(
        dataset="",
        epochs=10,
        image_h=256,
        image_w=256,
        batch_s=32
        ):

    print("binary classication model tuner")
    print()
    
    train_set_image, test_set_image = data_preprocessing(
        dataset=dataset,
        image_h=image_h,
        image_w=image_w,
        batch_s=batch_s
        )

    custom_objective = Objective("val_recall", direction="max")
    
    # Create a Keras Tuner RandomSearch tuner
    tuner = BayesianOptimization(
        (lambda x: model_build_tuner(x,image_h,image_w)),
        overwrite=True,
        objective=custom_objective,
        max_trials=10,
        directory='TuningMetadata',
        project_name='binaryclassification'
    )

    # Perform the hyperparameter search
    tuner.search(
        train_set_image,
        epochs=epochs,
        validation_data=test_set_image
    )

    # Get the best model
    best_model = tuner.get_best_models(1)[0]

    return best_model, None

# if __name__ == "__main__":
#     binary_classification()
