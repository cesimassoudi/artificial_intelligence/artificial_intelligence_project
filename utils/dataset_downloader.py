import glob
import os
import zipfile

from onedrivedownloader import download

data_dir = "data" # @param {type:"string"}s

def download_dataset():
    # dirty dataset
    download(url="https://viacesifr-my.sharepoint.com/:u:/g/personal/rayhann_massoudi_viacesi_fr/EcKF-o75vKVMucKwLmMk6UIBUGDpDVI8vtjz9Sr57MhFqA?e=vsvgW3", filename="ProjectDataset.zip", unzip=True,unzip_path=data_dir+"/dirty")
    
    # clean dataset
    download(url="https://viacesifr-my.sharepoint.com/:u:/g/personal/rayhann_massoudi_viacesi_fr/Ebq9jW22wc1Nqy5qLRDJM54B0NfhWGQsibUfctJgDT-5rw?e=dqFpcy", filename="ProjectDatasetClean.zip", unzip=True,unzip_path=data_dir+"/clean")

    # noisy dataset
    download(url="https://viacesifr-my.sharepoint.com/:u:/g/personal/rayhann_massoudi_viacesi_fr/EWy_WA9GPqJEsHXqRonTRCMBt_qkcQiAfApZECooLekHgQ?e=UUoRdW", filename="ProjectDatasetNoisy.zip", unzip=True,unzip_path=data_dir+"/noisy")
    

def extract_livrable1():
    download_dataset()

    dataset_list = ["clean", "dirty", "noisy"]

    for dataset in dataset_list:
        # Define the directory path 
        directory_path = data_dir+'/'+dataset

        # Find all files containing '2' in their filenames in the specified directory
        files_to_remove = glob.glob(os.path.join(directory_path, '*2*.zip'))

        # Iterate through the list of files and remove them
        for file_path in files_to_remove:
            try:
                os.remove(file_path)
                print(f"File {file_path} removed successfully.")
            except OSError as e:
                print(f"Error: {e} - {file_path}")

        print("Removal process completed.")

        extension = ".zip"

        for item in os.listdir('./'+directory_path):  # loop through items in dir
            if item.endswith(extension): # check for ".zip" extension
                print('extracting '+directory_path+'/'+item)
                file_name = os.path.abspath(directory_path+'/'+item) # get full path of files
                zip_ref = zipfile.ZipFile(file_name) # create zipfile object
                zip_ref.extractall(directory_path)  # extract file to dir
                zip_ref.close() # close file
                os.remove(file_name) # delete zipped file
