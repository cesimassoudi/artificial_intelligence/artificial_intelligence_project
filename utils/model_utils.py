import os
import matplotlib.pyplot as plt
import tensorflow as tf
from datetime import datetime
from pathlib import Path

def change_parameters():
    default_dataset = "noisy"
    image_h = 256
    image_w = 256
    batch_s = 32 
    epochs  = 10

    print("Wich dataset do you want to use (dirty, clean, noisy) (Default: noisy):")
    ninput = input()

    if ninput == "clean" or ninput == "dirty" or ninput == "noisy":
        default_dataset = ninput

    print(f" How many epochs to train on (Default : {epochs}):")
    ninput = input()

    if ninput.isdigit():
        epochs = int(ninput)

    print(f" How many batch ? (Default : {batch_s}):")
    ninput = input()

    if ninput.isdigit():
        batch_s = int(ninput)
    
    print(f" Which image height ? (Default : {image_h}):")
    ninput = input()

    if ninput.isdigit():
        image_h = int(ninput)

    print(f" Which image height ? (Default : {image_w}):")
    ninput = input()

    if ninput.isdigit():
        image_w = int(ninput)

    return default_dataset, epochs, image_h, image_w, batch_s

def load_model():

    print("Which model do you want to load:")
    
    listModel = [ directory for directory in  os.listdir("output") if os.path.isdir("output/"+directory) ]

    for i in range(len(listModel)):
        model_name = listModel[i].split(sep="/")[-1]
        print(f"({i}) - {model_name}")

    name = listModel[int(input())]
    
    filename = f"output/{name}/Model.h5"

    path = Path(filename)

    if os.path.exists(path):
        model = tf.keras.models.load_model(path)

        show_summary(model)
        
        return name, model
    else :
        print("Your model does not exists")
        return "", None

def save_model(model, history=None, metric="recall"):
    print("Which name do you want to give to your model:")
    name = input()
    filename = f"output/{name}/Model.h5"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    
    model.save(filename)

    save_summary(model,name)
    if history != None:
        show_graph(history, metric, name=name)
        # show_graph(history, metric, name=name, center=True)

    return name

def save_summary_fn(information,name):
    print(information)
    filename = f"output/{name}/Summary.txt"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    
    text_file = open(filename, "a")
    text_file.write(information+'\n')

    text_file.close()

def show_summary(model):
    
    model.summary(
        expand_nested=True,
        show_trainable=True
    )

def save_summary(model,name):
    
    model.summary(
        print_fn=(lambda x: save_summary_fn(x,name)),
        expand_nested=True,
        show_trainable=True
    )


def dataset_to_labellist(dataset):
    listbatch = [batch for batch in dataset]
    labellist = []

    for i in listbatch:
        labelsArrays = [ tensor.numpy() for tensor in i[1:]]
        labellist.extend([[sub_array[i] for sub_array in labelsArrays] for i in range(len(labelsArrays[0]))])
    return labellist

def show_error(model,test_dataset,labels=None,max_toshow = 5):
    ErrorCount = 0
    ErrorIndex = []
    ErrorValue = []

    if labels == None:
        labels = dataset_to_labellist(test_dataset)

    results = model.predict(test_dataset)
    
    for i in range(len(results)):
        result = [ round(j) for j in results[i]]
        label = labels[i]

        if any(round(result[j]) != round(label[j]) for j in range(len(result))):
            ErrorCount += 1
            if ErrorCount < max_toshow:
                ErrorIndex.append(i)
                ErrorValue.append((result,label))
    
    listbatch = [batch for batch in test_dataset]
    valuelist = []

    for i in listbatch:
        valuelist.extend(i[0].numpy())

    for i in range(len(ErrorIndex)):
        plt.imshow(valuelist[ErrorIndex[i]]/255)
        plt.show()
        print(f"Error: Expected {ErrorValue[i][1]} Got {ErrorValue[i][0]}")

    print(ErrorCount/len(results))
    
def show_graph(
        history, 
        metrics: str="", 
        name:str="", 
        center:bool=False,
        ):
    print("graph showned")
    print()

    rec, val_rec, loss, val_loss = [], [], [], []


    loss = history.history['loss']
    val_loss = history.history['val_loss']
    
    epochs_range = range(len(loss))

    if metrics != "":
        plt.subplot(1, 2, 2)
    
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')

    if center:
        plt.ylim(0, 1)

    if metrics != "":
        plt.figure(figsize=(16, 8))
        plt.subplot(1, 2, 1)
        plt.legend(loc='lower right')
        plt.title('Training and Validation ' + metrics)
        if center:
            plt.ylim(0, 1)


        if metrics == "recall":
            rec = history.history['recall']
            val_rec = history.history['val_recall']

            plt.plot(epochs_range, rec, label='Training Recall')
            plt.plot(epochs_range, val_rec, label='Validation Recall')

        elif metrics == "accuracy":
            rec = history.history['accuracy']
            val_rec = history.history['val_accuracy']

            plt.plot(epochs_range, rec, label='Training Accuracy')
            plt.plot(epochs_range, val_rec, label='Validation Accuracy')

    if name != "":
        if metrics == "":
            plt.savefig("./output/"+name+"/graph_loss.png")
        else:
            if center:
                plt.savefig("./output/"+name+"/graph_center.png")
            else:
                plt.savefig("./output/"+name+"/graph.png")

    plt.show()


def show_noise_res(
        true_image, 
        noise_image, 
        predict_image, 
        nb_img=10,
        name:str=""
        ):
    
    print("show noise image")

    plt.figure(figsize=(20, 4))

    for img in true_image:
        for i in range(nb_img):
            ax = plt.subplot(3, nb_img, i + 1)
            plt.imshow(img[i])
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        # print(tensor)
        # print(type(tensor))
        # print(tensor.numpy().shape)

    for noise in noise_image:
        for i in range(nb_img):
            ax = plt.subplot(3, nb_img, i + 1 + nb_img)
            plt.imshow(noise[i])
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

    i = 0
    for predict in predict_image:
        ax = plt.subplot(3, nb_img, i + 1 + nb_img * 2)
        plt.imshow(predict)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        i += 1 

    if name != "":
        time_str = str(int(datetime.utcnow().timestamp()))
        plt.savefig("./output/"+name+"/prediction_"+time_str+".png")

    plt.show()

