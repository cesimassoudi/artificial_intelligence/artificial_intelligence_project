import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from models.binary_classification import binary_classification, binary_classification_tuner
from models.denoiser import denoiser, denoiser_tuner, get_noise_img
from utils.model_utils import show_graph, save_model, load_model, change_parameters, show_noise_res
from utils.dataset_downloader import extract_livrable1

import tensorflow as tf
tf.get_logger().setLevel('ERROR')

import sys

def flush_input():
    try:
        # For Python 3
        import msvcrt
        while msvcrt.kbhit():
            msvcrt.getch()
    except ImportError:
        # For other operating systems
        import termios
        termios.tcflush(sys.stdin, termios.TCIOFLUSH)


def banner():
    print("    _   ___                  _        _   ")
    print("   /_\ |_ _|__ _ __ _ _ ___ (_)___ __| |_ ")
    print("  / _ \ | |___| '_ \ '_/ _ \| / -_) _|  _|")
    print(" /_/ \_\___|  | .__/_| \___// \___\__|\__|")
    print("              |_|         |__/            ")
    print()

def clear_screen():
    os.system('cls')

def wait():
    print()
    print("Waiting for a input...")
    input()

def menu():
    chosen_model = None
    chosen_model_name = ""

    image_h = 256
    image_w = 256
    batch_s = 32 
    epochs  = 10
    default_dataset = "noisy"

    cpu_activated = False

    while True:
        clear_screen()
        banner()

        if chosen_model_name != "":
            print("Current Model : "+ chosen_model_name)
        else :
            print("No model chosen")
        print(f"Image dimension (h*w) : {image_h}*{image_w}")
        print(f"Batch size : {batch_s}")
        print(f"Epochs count : {epochs}")
        print(f"CPU augmented : {cpu_activated}")
        print(f"Default dataset : {default_dataset}")

        print()
        print("Choose the model you want to train:")
        print("(1) - Binary Classification")
        print("(2) - Binary Classification Tuner")
        print("(3) - Denoiser")
        print("(4) - Denoiser Tuner")
        print("(5) - Denoiser predict")
        # other model 
        print()
        print("Other action:")
        print("(d) - Download dataset")
        print("(l) - Load trained model")
        print("(p) - Change parameters ")
        if not cpu_activated:
            print("(a) - CPU aumgmentation")
        print("(u) - Unload model")
        print("(e) - Exit")

        userInput = input()

        match userInput:
            case "1":
                clear_screen()
                model, history = binary_classification(
                    model_binary_classification=chosen_model,
                    dataset=default_dataset,
                    epochs=epochs,
                    image_h=image_h,
                    image_w=image_w,
                    batch_s=batch_s
                    )
                # show_graph(history, "recall")
                # show_graph(history, "recall", True)
                flush_input()
                print("Do you want to save your model ? (y/[N])")
                input_save = input()
                if(input_save == "y"):
                    chosen_model = model
                    chosen_model_name = save_model(model, history)
                else:
                    show_graph(history, "recall")
                    # show_graph(history, "recall", center=True)
                # wait()
            case "2":
                clear_screen()
                model, history = binary_classification_tuner(
                    dataset=default_dataset,
                    epochs=epochs,
                    image_h=image_h,
                    image_w=image_w,
                    batch_s=batch_s
                    )
                flush_input()
                print("Do you want to save your model ? (y/[N])")
                input_save = input()
                if(input_save == "y"):
                    chosen_model = model
                    chosen_model_name = save_model(model, history)
                    wait()
            case "3":
                clear_screen()
                model, history = denoiser(
                    ae_Model=chosen_model,
                    dataset=default_dataset,
                    epochs=epochs,
                    image_h=image_h,
                    image_w=image_w,
                    batch_s=batch_s
                    )
                # show_graph(history, "recall")
                # show_graph(history, "recall", True)
                flush_input()
                print("Do you want to save your model ? (y/[N])")
                input_save = input()
                if(input_save == "y"):
                    chosen_model = model
                    chosen_model_name = save_model(model, history, metric="")
                else:
                    show_graph(history, "")
                    # show_graph(history, "", center=True)
            case "4":
                clear_screen()
                model, history = denoiser_tuner(
                    dataset=default_dataset,
                    epochs=epochs,
                    image_h=image_h,
                    image_w=image_w,
                    batch_s=batch_s
                    )
                flush_input()
                print("Do you want to save your model ? (y/[N])")
                input_save = input()
                if(input_save == "y"):
                    chosen_model = model
                    chosen_model_name = save_model(model, history)
                    wait()
            case "5":
                clear_screen()

                nb = 10
                print('How many photo do you want to predict ? (default: 10)')
                nb_input = input()
                if nb_input.isdigit():
                    nb = int(nb_input)

                seed = 42
                print('Do you want to change the random seed ? (default: 42)')
                seed_input = input()
                if seed_input.isdigit():
                    seed = int(seed_input)

                img, noise, predict = get_noise_img(
                    model=chosen_model,
                    nb=nb,
                    img_seed=seed
                    )
                
                if img == None:
                    wait()
                    break

                print('Do you want to save the prediction ? (y/[N])')
                input_save = input()
                if(input_save == "y"):
                    show_noise_res(
                        true_image=img, 
                        noise_image=noise, 
                        predict_image=predict, 
                        nb_img=nb,
                        name=chosen_model_name)
                else:
                    show_noise_res(img, noise, predict)
                wait()
            case "d":
                extract_livrable1()
                wait()
            case "a":
                if not cpu_activated:
                    activate_cpu_augment()
                    cpu_activated = True
                    wait()
            case "l":
                chosen_model_name, chosen_model = load_model()
                output_shape = chosen_model.layers[1].output_shape
                print(output_shape)
                image_h = output_shape[1]
                image_w = output_shape[2]
                wait()
            case "p":
                default_dataset, epochs, image_h, image_w, batch_s = change_parameters()
            case "u":
                chosen_model = None
                chosen_model_name = ""
            case "e":
                exit(1)
            # case _:
            #     # do nothing
        


def activate_cpu_augment():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Enable memory growth for each GPU
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)

def main():
    menu()


if __name__ == "__main__":
    main()